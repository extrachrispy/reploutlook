# reploutlook

Reply to Microsoft Outlook 2016 for Mac using MacVim to produce a traditional
quoted-text, inline reply, instead of top-posting garbage.  Although I do not
recommend it, you can easily change to emacs by editing the copy scpt files.

## Preparation

1. Drop the files in this repo into your ~/bin directory.
1. Open System Preferences, click Security & Privacy, and go to the Privacy
pane.
1. Click "Click the lock to make changes" on the lower left.
1. Click Accessiblity in the left pane.
1. On the right pane, click "+" and add and enable Terminal.app.

(Recommended):  choose the traditional "On date, so-and-so wrote" attribution
line in Outlook preferences.

## Invocation

Open the message to which you wish to reply in the viewer pane.  In
Terminal.app, type

    reploutlook

to reply to the sender, or

    reploutlook -a

to reply to all.  The copy scripts will select all the text in the message, copy
it, filter it through quotemail to add the ">" characters and wrap the text at
70 columns, and then send it to your editor.  When you save and quit in your
editor, the paste script will select all in the reply editor in Outlook and
paste your edited response.  At that point, you can send the message.

## Coming Soon

* Macports package
* Service for keybinding instead of manual invocation
