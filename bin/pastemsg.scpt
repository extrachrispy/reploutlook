#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <chris@behanna.org> wrote this file.  As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.   Chris BeHanna
# ----------------------------------------------------------------------------
#
tell application "Microsoft Outlook" to activate
tell application "System Events"
    tell process "Outlook"
        tell menu bar 1
            click menu bar item "Edit"'s menu "Edit"'s menu item "Select All"
            click menu bar item "Edit"'s menu "Edit"'s menu item "Paste"
        end tell
    end tell
end tell
